/*
    Narsx server is meant to serve as a back office for Narsx: the
    connected coding solution.

    Copyright (C) 2015  Narsx-Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
  * Check whether the HTTP method requires CSRF protection or not.
  *
  * @param method the HTTP method to check.
  * @return whether the HTTP method requires CSRF protection or not.
  */
function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

/**
  * Verify that the given URL is a same-origin URL.
  * The URL can be relative, absolute or scheme relative.
  *
  * @param url the URL to check.
  * @return whether the given URL is a same-origin URL.
  */
function sameOrigin(url) {
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        !(new RegExp("^(\/\/|http:|https:).*").test(url));
}

/**
  * Get the content of a cookie with the given name.
  *
  * @param name the name of the cookie to retrieve the content from
  * @return the value of the cookie.
  */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);

            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }

    return cookieValue;
}

/**
* Retrieve the CSRF token from the cookies and add it to the HTTP Header
* before performing an ajax request.
*
* @param callback the ajax call to execute.
*/
function setCsrfToken(callback) {
    var csrfToken = getCookie('csrftoken');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                xhr.setRequestHeader("X-CSRFToken", csrfToken);
            }
        }
    });

    callback();
}

/**
* Invite a given user to the document with the given id.
*
* @param username the username of the user to invite.
* @param documentId the ID of the document.
* @param successCallback the function called if the call is a success.
* @param error the function called if the call returns an error. The function is given an error msg as arg.
*/
function inviteUserToDocument(username, documentId, successCallback, errorCallback) {
    setCsrfToken(function() {
        $.ajax({
            url: '/invite/' + documentId + '/',
            type: 'POST',
            data: {
                'invitee': username
            },
            success: function(result) {
                result = $.parseJSON(result);

                if (result.status != undefined) {
                    if (result.status == "error") {
                        errorCallback(result["message"]);
                    } else {
                        successCallback();
                    }
                } else {
                    errorCallback("Unknown server error");
                }
            },
            error: function(error, status) {
                errorCallback(status + "\n" + error);
            }
        });
    });
}


/**
* Get the list of all usernames subscribed on the service as a JSON array.
*
* @param success the function called if the call is a success. The resulting JSON array is given as param to this func.
* @param error the function called if the call returns an error. The function is given an error msg as arg.
*/
function getAllUsernames(success, error) {
    setCsrfToken(function() {
        $.ajax({
            url: '/userlist/',
            type: 'GET',
            success: function(result) {
                result = $.parseJSON(result);

                if (result["user"] != undefined) {
                  success(result["user"]);
                } else {
                    error("Server error");
                }
            },
            error: function(error, status) {
                error(status + "\n" + error);
            }
        });
    });
}

/**
* Send the Websocket token of the connected user to the server to store it.
*
* @param token The Websocket token given by the token server.
* @param successCallback The function called if the call is a success.
* @param errorCallback The function called if the call returns an error. The function is given an error msg as arg.
*/
function setTokenForCurrentUser(token, successCallback, errorCallback) {
    setCsrfToken(function() {
        $.ajax({
            url: '/settoken/',
            type: 'GET',
            data: {
                'token': token
            },
            success: function(result) {
                result = $.parseJSON(result);
                
                if (result["status"] != undefined) {
                    if (result["status"] == "error") {
                        error(result["message"]);
                    } else {
                        successCallback();
                    }
                } else {
                    errorCallback("Server error");
                }
            },
            error: function(error, status) {
                errorCallback(status + "\n" + error);
            }
        });
    });
}