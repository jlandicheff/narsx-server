/*
    Narsx server is meant to serve as a back office for Narsx: the
    connected coding solution.

    Copyright (C) 2015  Narsx-Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$(document).ready(function() {

	// Invite another user to edit the document
	$("#invite_btn").click(function() {
		inviteUserToDocument(
			$("#invite").val(),
			$("#documentid").val(),
			function() {
				$("#invite").val("");
				alert("SUCCESS");
			},
			function(errorMessage) {
				alert(errorMessage);
			}
		);
	});
});

