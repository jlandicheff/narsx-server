"""
    Narsx server is meant to serve as a back office for Narsx: the
    connected coding solution.

    Copyright (C) 2015  Narsx-Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.conf.urls import include, url
from django.contrib import admin
from narsx import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index),
    url(r'^login/$', views.login),
    url(r'^contactus/$', views.contactus),
    url(r'^signin/$', views.signin),
    url(r'^signout/$', views.signout),
    url(r'^userlist/$', views.user_list),
    url(r'^settoken/$', views.set_token),
    url(r'^new/$', views.new),
    url(r'^show/(?P<document_id>\d+)/$', views.show),
    url(r'^invite/(?P<document_id>\d+)/$', views.invite),
]
